try {
  const input = document.querySelector(".select-input");
  const contactBlock = document.querySelector(".contact-us");
  const listForContact = document.querySelector(".contact-list");
  const listPoints = document.querySelectorAll(".point");
  const tel = document.querySelector(".tel");
  const phoneContainer = document.querySelector(".phone-container");
  if (!input || !contactBlock || !listForContact || !listPoints || !tel) {
    throw new Error();
  }
  contactBlock.addEventListener("click", e => {
    if (e.target === input && !listForContact.classList.contains("active")) {
      listForContact.classList.add("active");
    } else {
      listForContact.classList.remove("active");
    }
  });
  input.addEventListener("click", () => {
    tel.classList.remove("active");
    phoneContainer.classList.toggle("active");
  });
  listForContact.addEventListener("click", e => {
    listForContact.classList.remove("active");
    tel.classList.add("active");

    let selectResult = e.target.innerText;
    function showPhone(parent, phone) {
      phoneContainer.classList.add("active");
      parent.classList.add("active");
      parent.innerHTML = `<p class="select-result">${selectResult}:</p><i class='fas fa-phone-alt select-phone'></i> ${phone}`;
    }
    switch (e.target) {
      case listPoints[0]:
        showPhone(tel, "+38 044 242 27 01");
        break;
      case listPoints[1]:
        showPhone(tel, "+38 044 242 27 01");
        break;
      case listPoints[2]:
        showPhone(tel, "+38 044 242 09 97");
        break;
      case listPoints[3]:
        showPhone(tel, "+38 044 242 09 97");
        break;
      case listPoints[4]:
        showPhone(tel, "+38 044 242 09 97");
        break;
      case listPoints[5]:
        showPhone(tel, "+38 044 242 27 01");
        break;
      case listPoints[6]:
        showPhone(tel, "+38 044 242 09 97");
        break;
    }
  });
} catch (e) {
  // console.log(e);
}
