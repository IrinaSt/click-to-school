try {
  const burgerMenuBtn = document.querySelector(".header-menu__burger-btn");
  const burgerMenu = document.querySelector(".header-menu__burger-menu");
  const burgerMenuClose = document.querySelector(".header-menu__close-btn");
  const burgerSubMenuTeachProg = document.querySelector(
    ".submenu-teach-programm"
  );
  const burgerSubMenuTimatable = document.querySelector(".submenu-timetable");
  if (
    !burgerMenuBtn ||
    !burgerMenu ||
    !burgerSubMenuTeachProg ||
    !burgerSubMenuTimatable
  ) {
    throw new Error();
  }
  burgerMenuBtn.addEventListener("click", () => {
    burgerMenu.classList.add("active");
    burgerMenuClose.style.opacity = "1";
    if (window.screen.height + window.screen.width <= 1140)
      document.body.style.overflow = "hidden";
  });

  document.body.addEventListener("click", e => {
    if (
      !e.target.classList.contains("header-menu__burger-btn") &&
      !e.target.classList.contains("header-menu__burger-btn-letters") &&
      !e.target.classList.contains("header-menu__burger-menu")
    ) {
      if (burgerMenu.classList.contains("active")) {
        burgerMenu.classList.remove("active");
        burgerMenuClose.style.opacity = "0";
        document.body.style.overflow = "auto";
      }
    }
  });

  burgerSubMenuTeachProg.onclick = e => {
    if (!e.target.classList.contains("teach-programm-active")) {
      dropSubMenuActive(e, "teach-programm");
    } else {
      dropSubMenuDisactive(e, "teach-programm");
    }
  };
  burgerSubMenuTimatable.onclick = e => {
    if (!e.target.classList.contains("timetable-active")) {
      dropSubMenuActive(e, "timetable");
    } else {
      dropSubMenuDisactive(e, "timetable");
    }
  };

  function dropSubMenuActive(e, className) {
    burgerMenu.classList.add("active");
    e.target.classList.remove(`submenu-${className}`);
    e.stopPropagation();
    e.preventDefault();
    e.target.classList.add(`${className}-active`);
    e.target.childNodes[0].className = "fas fa-caret-down";
    document
      .querySelectorAll(`.${className}`)
      .forEach(el => el.classList.add("active"));
  }
  function dropSubMenuDisactive(e, className) {
    debugger;
    burgerMenu.classList.add("active");
    e.preventDefault();
    e.stopPropagation();
    e.target.classList.remove(`${className}-active`);
    e.target.childNodes[0].className = "fas fa-caret-right";
    document
      .querySelectorAll(`.${className}`)
      .forEach(el => el.classList.remove("active"));
    e.target.classList.add(`submenu-${className}`);
  }
} catch (e) {
  // console.log(e);
}
