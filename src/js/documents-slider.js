try {
  const tabs = document.querySelector(".tabs");
  const tab = Array.from(document.querySelectorAll(".tab"));
  const nextTab = document.querySelector(".docs-arrow-next");
  const prevTab = document.querySelector(".docs-arrow-prev");
  const slideContent = [...document.querySelectorAll(".slide")];

  tabs.addEventListener("click", e => {
    if (Array.from(e.target.classList).includes("tab")) {
      tab.forEach(el => el.classList.remove("active"));
      e.target.classList.add("active");
      slideContent.forEach(el => {
        el.classList.remove("active");
        if (el.dataset.slider === e.target.dataset.slider) {
          console.log(el, e.target);
          el.classList.add("active");
        }
      });
    }
  });

  const allTabs = [...tab];
  let leftBorder = 0;
  let rightBorder = 4;

  if (leftBorder === 0) {
    prevTab.classList.add("btn-disable");
  }

  nextTab.addEventListener("click", () => {
    prevTab.classList.remove("btn-disable");

    if (leftBorder + 1 === 0) {
      prevTab.classList.add("btn-disable");
    }
    if (rightBorder + 1 === 6) {
      nextTab.classList.add("btn-disable");
    }

    if (!allTabs[leftBorder] || !allTabs[rightBorder]) {
      return;
    }
    allTabs[leftBorder].classList.add("hide-tab");
    allTabs[rightBorder].classList.remove("hide-tab");
    leftBorder++;
    rightBorder++;
  });

  prevTab.addEventListener("click", () => {
    nextTab.classList.remove("btn-disable");

    if (leftBorder - 1 === 0) {
      prevTab.classList.add("btn-disable");
    }
    if (rightBorder - 1 === 6) {
      nextTab.classList.add("btn-disable");
    }
    leftBorder--;
    rightBorder--;
    if (!allTabs[leftBorder] || !allTabs[rightBorder]) {
      return;
    }

    allTabs[leftBorder].classList.remove("hide-tab");
    allTabs[rightBorder].classList.add("hide-tab");
    if (leftBorder === 0 || rightBorder === 0) {
      return;
    }
  });
} catch (e) {}
