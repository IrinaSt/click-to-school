try {
  function sliderBuild(activeSlide) {
    activeSlide.previousElementSibling.classList.add("near-active");
    activeSlide.nextElementSibling.classList.add("near-active");
  }
  function clearNearActive(activeSlide) {
    activeSlide.previousElementSibling.classList.remove("near-active");
    activeSlide.nextElementSibling.classList.remove("near-active");
  }
  const slider = document.querySelector("#news-slider");
  const activeSlide = document.querySelector("#news-slider .slide.active");
  sliderBuild(activeSlide);
  const leftArrow = document.querySelector(".slider>.arrow__left");
  const rightArrow = document.querySelector(".slider>.arrow__right");
  if (!slider || !activeSlide || !leftArrow || !rightArrow) {
    throw new Error();
  }
  leftArrow.addEventListener("click", slideToLeft);
  rightArrow.addEventListener("click", slideToRight);
  function slideToRight() {
    const activeSlide = document.querySelector("#news-slider .slide.active");
    activeSlide.nextElementSibling.classList.add("active");
    activeSlide.classList.remove("active");
    clearNearActive(activeSlide);
    sliderBuild(document.querySelector("#news-slider .slide.active"));
    const firstSlide = document.querySelector(
      "#news-slider>.slide:first-child"
    );
    slider.append(firstSlide);
  }
  function slideToLeft() {
    const activeSlide = document.querySelector("#news-slider .slide.active");
    activeSlide.previousElementSibling.classList.add("active");
    activeSlide.classList.remove("active");
    clearNearActive(activeSlide);
    sliderBuild(document.querySelector("#news-slider .slide.active"));
    const lastSlide = document.querySelector("#news-slider>.slide:last-child");
    slider.prepend(lastSlide);
  }
} catch (e) {
  // console.log(e);
}
