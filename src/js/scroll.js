try {
  const buttons = document.querySelectorAll(".scroll-to-btn");
  if (!buttons.length) throw new Error();
  [...buttons].forEach(el => {
    el.onclick = e => {
      e.preventDefault();
      if (!document.querySelector(".photo-gallery")) {
        return;
      }
      if (e.target.classList.contains("gallery-btn")) {
        document
          .querySelector(".photo-gallery")
          .scrollIntoView({ behavior: "smooth" });
      } else if (e.target.classList.contains("team-btn")) {
        document
          .querySelector(".our-team")
          .scrollIntoView({ behavior: "smooth" });
      } else if (
        e.target.classList.contains("teach-btn") ||
        e.target.classList.contains("schedule-btn")
      ) {
        document
          .querySelector(".documents")
          .scrollIntoView({ behavior: "smooth" });
      }
    };
  });
} catch (e) {}
