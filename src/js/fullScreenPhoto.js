try {
  const photos = [...document.querySelectorAll(".photos-item")];
  const bigPhoto = document.querySelector(".big-photo");
  const cross = document.querySelector(".big-photo-close");
  photos.forEach(el => {
    el.onclick = () => {
      const photoCopy = `<img class="photos-item-big" src="${el.src}" alt="first bell">`;
      bigPhoto.innerHTML = photoCopy;
      bigPhoto.classList.add("active");
      cross.classList.add("active");
    };
  });
  document.onclick = e => {
    if (
      e.target.classList.contains("big-photo") ||
      e.target.classList.contains("big-photo-close")
    ) {
      bigPhoto.innerHTML = "";
      bigPhoto.classList.remove("active");
      cross.classList.remove("active");
    }
  };
} catch (e) {}
