try {
  const preschoolContainer = document.querySelector(
    ".preschool-department-main-content"
  );
  if (!preschoolContainer) {
    throw new Error();
  }
  preschoolContainer.onclick = ({ target }) => {
    if (
      Array.from(target.classList).includes(
        "preschool-department-schedule-title"
      )
    ) {
      Array.from(target.nextElementSibling.classList).includes("active")
        ? target.nextElementSibling.classList.remove("active")
        : target.nextElementSibling.classList.add("active");
    }
  };
} catch (e) {
  // console.log(e);
}
