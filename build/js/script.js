try {
  const input = document.querySelector(".select-input");
  const contactBlock = document.querySelector(".contact-us");
  const listForContact = document.querySelector(".contact-list");
  const listPoints = document.querySelectorAll(".point");
  const tel = document.querySelector(".tel");
  const phoneContainer = document.querySelector(".phone-container");
  if (!input || !contactBlock || !listForContact || !listPoints || !tel) {
    throw new Error();
  }
  contactBlock.addEventListener("click", e => {
    if (e.target === input && !listForContact.classList.contains("active")) {
      listForContact.classList.add("active");
    } else {
      listForContact.classList.remove("active");
    }
  });
  input.addEventListener("click", () => {
    tel.classList.remove("active");
    phoneContainer.classList.toggle("active");
  });
  listForContact.addEventListener("click", e => {
    listForContact.classList.remove("active");
    tel.classList.add("active");

    let selectResult = e.target.innerText;
    function showPhone(parent, phone) {
      phoneContainer.classList.add("active");
      parent.classList.add("active");
      parent.innerHTML = `<p class="select-result">${selectResult}:</p><i class='fas fa-phone-alt select-phone'></i> ${phone}`;
    }
    switch (e.target) {
      case listPoints[0]:
        showPhone(tel, "+38 044 242 27 01");
        break;
      case listPoints[1]:
        showPhone(tel, "+38 044 242 27 01");
        break;
      case listPoints[2]:
        showPhone(tel, "+38 044 242 09 97");
        break;
      case listPoints[3]:
        showPhone(tel, "+38 044 242 09 97");
        break;
      case listPoints[4]:
        showPhone(tel, "+38 044 242 09 97");
        break;
      case listPoints[5]:
        showPhone(tel, "+38 044 242 27 01");
        break;
      case listPoints[6]:
        showPhone(tel, "+38 044 242 09 97");
        break;
    }
  });
} catch (e) {
  // console.log(e);
}

try {
  const tabs = document.querySelector(".tabs");
  const tab = Array.from(document.querySelectorAll(".tab"));
  const nextTab = document.querySelector(".docs-arrow-next");
  const prevTab = document.querySelector(".docs-arrow-prev");
  const slideContent = [...document.querySelectorAll(".slide")];

  tabs.addEventListener("click", e => {
    if (Array.from(e.target.classList).includes("tab")) {
      tab.forEach(el => el.classList.remove("active"));
      e.target.classList.add("active");
      slideContent.forEach(el => {
        el.classList.remove("active");
        if (el.dataset.slider === e.target.dataset.slider) {
          console.log(el, e.target);
          el.classList.add("active");
        }
      });
    }
  });

  const allTabs = [...tab];
  let leftBorder = 0;
  let rightBorder = 4;

  if (leftBorder === 0) {
    prevTab.classList.add("btn-disable");
  }

  nextTab.addEventListener("click", () => {
    prevTab.classList.remove("btn-disable");

    if (leftBorder + 1 === 0) {
      prevTab.classList.add("btn-disable");
    }
    if (rightBorder + 1 === 6) {
      nextTab.classList.add("btn-disable");
    }

    if (!allTabs[leftBorder] || !allTabs[rightBorder]) {
      return;
    }
    allTabs[leftBorder].classList.add("hide-tab");
    allTabs[rightBorder].classList.remove("hide-tab");
    leftBorder++;
    rightBorder++;
  });

  prevTab.addEventListener("click", () => {
    nextTab.classList.remove("btn-disable");

    if (leftBorder - 1 === 0) {
      prevTab.classList.add("btn-disable");
    }
    if (rightBorder - 1 === 6) {
      nextTab.classList.add("btn-disable");
    }
    leftBorder--;
    rightBorder--;
    if (!allTabs[leftBorder] || !allTabs[rightBorder]) {
      return;
    }

    allTabs[leftBorder].classList.remove("hide-tab");
    allTabs[rightBorder].classList.add("hide-tab");
    if (leftBorder === 0 || rightBorder === 0) {
      return;
    }
  });
} catch (e) {}

try {
  const photos = [...document.querySelectorAll(".photos-item")];
  const bigPhoto = document.querySelector(".big-photo");
  const cross = document.querySelector(".big-photo-close");
  photos.forEach(el => {
    el.onclick = () => {
      const photoCopy = `<img class="photos-item-big" src="${el.src}" alt="first bell">`;
      bigPhoto.innerHTML = photoCopy;
      bigPhoto.classList.add("active");
      cross.classList.add("active");
    };
  });
  document.onclick = e => {
    if (
      e.target.classList.contains("big-photo") ||
      e.target.classList.contains("big-photo-close")
    ) {
      bigPhoto.innerHTML = "";
      bigPhoto.classList.remove("active");
      cross.classList.remove("active");
    }
  };
} catch (e) {}

try {
  const burgerMenuBtn = document.querySelector(".header-menu__burger-btn");
  const burgerMenu = document.querySelector(".header-menu__burger-menu");
  const burgerMenuClose = document.querySelector(".header-menu__close-btn");
  const burgerSubMenuTeachProg = document.querySelector(
    ".submenu-teach-programm"
  );
  const burgerSubMenuTimatable = document.querySelector(".submenu-timetable");
  if (
    !burgerMenuBtn ||
    !burgerMenu ||
    !burgerSubMenuTeachProg ||
    !burgerSubMenuTimatable
  ) {
    throw new Error();
  }
  burgerMenuBtn.addEventListener("click", () => {
    burgerMenu.classList.add("active");
    burgerMenuClose.style.opacity = "1";
    if (window.screen.height + window.screen.width <= 1140)
      document.body.style.overflow = "hidden";
  });

  document.body.addEventListener("click", e => {
    if (
      !e.target.classList.contains("header-menu__burger-btn") &&
      !e.target.classList.contains("header-menu__burger-btn-letters") &&
      !e.target.classList.contains("header-menu__burger-menu")
    ) {
      if (burgerMenu.classList.contains("active")) {
        burgerMenu.classList.remove("active");
        burgerMenuClose.style.opacity = "0";
        document.body.style.overflow = "auto";
      }
    }
  });

  burgerSubMenuTeachProg.onclick = e => {
    if (!e.target.classList.contains("teach-programm-active")) {
      dropSubMenuActive(e, "teach-programm");
    } else {
      dropSubMenuDisactive(e, "teach-programm");
    }
  };
  burgerSubMenuTimatable.onclick = e => {
    if (!e.target.classList.contains("timetable-active")) {
      dropSubMenuActive(e, "timetable");
    } else {
      dropSubMenuDisactive(e, "timetable");
    }
  };

  function dropSubMenuActive(e, className) {
    burgerMenu.classList.add("active");
    e.target.classList.remove(`submenu-${className}`);
    e.stopPropagation();
    e.preventDefault();
    e.target.classList.add(`${className}-active`);
    e.target.childNodes[0].className = "fas fa-caret-down";
    document
      .querySelectorAll(`.${className}`)
      .forEach(el => el.classList.add("active"));
  }
  function dropSubMenuDisactive(e, className) {
    debugger;
    burgerMenu.classList.add("active");
    e.preventDefault();
    e.stopPropagation();
    e.target.classList.remove(`${className}-active`);
    e.target.childNodes[0].className = "fas fa-caret-right";
    document
      .querySelectorAll(`.${className}`)
      .forEach(el => el.classList.remove("active"));
    e.target.classList.add(`submenu-${className}`);
  }
} catch (e) {
  // console.log(e);
}

try {
  function sliderBuild(activeSlide) {
    activeSlide.previousElementSibling.classList.add("near-active");
    activeSlide.nextElementSibling.classList.add("near-active");
  }
  function clearNearActive(activeSlide) {
    activeSlide.previousElementSibling.classList.remove("near-active");
    activeSlide.nextElementSibling.classList.remove("near-active");
  }
  const slider = document.querySelector("#news-slider");
  const activeSlide = document.querySelector("#news-slider .slide.active");
  sliderBuild(activeSlide);
  const leftArrow = document.querySelector(".slider>.arrow__left");
  const rightArrow = document.querySelector(".slider>.arrow__right");
  if (!slider || !activeSlide || !leftArrow || !rightArrow) {
    throw new Error();
  }
  leftArrow.addEventListener("click", slideToLeft);
  rightArrow.addEventListener("click", slideToRight);
  function slideToRight() {
    const activeSlide = document.querySelector("#news-slider .slide.active");
    activeSlide.nextElementSibling.classList.add("active");
    activeSlide.classList.remove("active");
    clearNearActive(activeSlide);
    sliderBuild(document.querySelector("#news-slider .slide.active"));
    const firstSlide = document.querySelector(
      "#news-slider>.slide:first-child"
    );
    slider.append(firstSlide);
  }
  function slideToLeft() {
    const activeSlide = document.querySelector("#news-slider .slide.active");
    activeSlide.previousElementSibling.classList.add("active");
    activeSlide.classList.remove("active");
    clearNearActive(activeSlide);
    sliderBuild(document.querySelector("#news-slider .slide.active"));
    const lastSlide = document.querySelector("#news-slider>.slide:last-child");
    slider.prepend(lastSlide);
  }
} catch (e) {
  // console.log(e);
}

try {
  const preschoolContainer = document.querySelector(
    ".preschool-department-main-content"
  );
  if (!preschoolContainer) {
    throw new Error();
  }
  preschoolContainer.onclick = ({ target }) => {
    if (
      Array.from(target.classList).includes(
        "preschool-department-schedule-title"
      )
    ) {
      Array.from(target.nextElementSibling.classList).includes("active")
        ? target.nextElementSibling.classList.remove("active")
        : target.nextElementSibling.classList.add("active");
    }
  };
} catch (e) {
  // console.log(e);
}

try {
  const buttons = document.querySelectorAll(".scroll-to-btn");
  if (!buttons.length) throw new Error();
  [...buttons].forEach(el => {
    el.onclick = e => {
      e.preventDefault();
      if (!document.querySelector(".photo-gallery")) {
        return;
      }
      if (e.target.classList.contains("gallery-btn")) {
        document
          .querySelector(".photo-gallery")
          .scrollIntoView({ behavior: "smooth" });
      } else if (e.target.classList.contains("team-btn")) {
        document
          .querySelector(".our-team")
          .scrollIntoView({ behavior: "smooth" });
      } else if (
        e.target.classList.contains("teach-btn") ||
        e.target.classList.contains("schedule-btn")
      ) {
        document
          .querySelector(".documents")
          .scrollIntoView({ behavior: "smooth" });
      }
    };
  });
} catch (e) {}

try {
  const newsSlider = new Swiper(".swiper-news-container", {
    effect: "coverflow",
    grabCursor: true,
    centeredSlides: true,
    initialSlide: 2,
    slidesPerView: "auto",
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true
    }
  });
  if (!newsSlider) throw new Error();
} catch (e) {}
